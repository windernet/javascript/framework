# WinderNet JavaScript Framework

The WinderNet JavaScript Framework is (like the name says) a framework for JavaScript. It is performant, customizable and runs with plain JavaScript; no third party packages are used.

### Installation
For now, download the framework and place it into the vendor directory of your project. Proper ways of installation (e.g. per [NPM](https://www.npmjs.com/)) will be provided in future.

### Usage
In your code, include the files you want to use. A proper startpoint to call the whole framework will be added in future.

### Support
For bugs and requests, please use the [GitLab issue tracker](https://gitlab.com/windernet/javascript/framework/-/issues). For questsions, please use the [GitLab wiki](https://gitlab.com/windernet/javascript/framework/-/wikis/home) or see in file documentation.

### License
This project is licensed under the **MIT License**. [Learn more](https://opensource.org/license/mit/)
